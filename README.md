## Projeto
Criado módulo de RSVP no Curso - Drupal 8 - Module Development Course

## Tecnologia
Utilizada a versão do Drupal 8.9.16

## Modo de instalação
* Copiar a pasta 'rsvplist' para o repositório 'modules/custom/';
* No painel administrativo do Drupal, no menu 'Extend', instalar o 'RSVP List';

## Como usar
* No painel administrativo do Drupal, clicar no menu 'Content', criar um 'Artigo' e nessa mesma tela procurar a opção 'RSVP COLLECTION', selecionar o 'Collect RSVP e-mail addresses for this node.' e salvar;
* No painel administrativo do Drupal, clicar no menu 'Structure', em 'Block layout', escolher o local que desejar exibir o bloco de RSVP, clicar em 'Place block' e escolha o 'RSVP Block';
* Para visualizar o relatório com os e-mails de RSVP cadastrados, ir no painel administrativo do Drupal, clicar no menu 'Reports' e na opção 'List of RSVP submissions';
* Caso deseje alterar as permissões de visualização, ir no painel administrativo do Drupal, clicar no menu 'People', clicar em 'Permissions' e procurar por 'RSVP List';
